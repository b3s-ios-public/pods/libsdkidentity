#!/bin/sh

pod repo push mypod-spec libSDKIdentity.podspec \
					--allow-warnings \
					--use-libraries \
					--skip-tests  \
					--skip-import-validation \
					--no-private \
					--verbose
