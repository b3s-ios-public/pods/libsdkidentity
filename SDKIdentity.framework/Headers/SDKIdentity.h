//
//  SDKIdentity.h
//
//  Created by cheng on 2019/11/12.
//  Copyright © 2019 zhimei.ai. All rights reserved.
//

#ifndef SDKIdentity_h
#define SDKIdentity_h

#import <Foundation/Foundation.h>

@interface SDKIdentity : NSObject

+ (id)sharedInstance;
- (NSString*)getData;
- (NSString*)getIdentity;
- (NSString*)getIdOpen;
@end

#endif /* SDKIdentity_h */
